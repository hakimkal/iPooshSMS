package org.lepsms.ipooshsms;

/**
 * Made by Andre Esteves
 * Interface to listen to sms
 */
public interface SmsListener {
    void messageReceived(String messageText, String senderPhone);
}