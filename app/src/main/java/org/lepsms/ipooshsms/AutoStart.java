package org.lepsms.ipooshsms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


/**
 * Made by Andre Esteves
 * Auto start when reboot
 */
public class AutoStart extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intentParam) {
        Intent intent = new Intent(context,SmsService.class);
        context.startService(intent);
        Log.i("Autostart", "started");
    }
}
