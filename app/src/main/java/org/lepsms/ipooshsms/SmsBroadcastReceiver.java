package org.lepsms.ipooshsms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;



public class SmsBroadcastReceiver extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";
    private static SmsListener mListener;

    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        String smsBody = "";
        String address = "";
        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";
            for (int i = 0; i < sms.length; ++i) {
                String format = intentExtras.getString("format");
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i], format);

                smsBody = smsMessage.getMessageBody().toString();
                address = smsMessage.getOriginatingAddress();

                smsMessageStr += "SMS From: " + address + "\n";
                smsMessageStr += smsBody + "\n";
            }


            if (!smsBody.equals("")) {

                Log.e("Message to send", smsBody);
                Log.e("Message from", address);
              //  mListener.messageReceived(smsBody, address);
            }


            MainActivity inst = MainActivity.instance();
            inst.updateInbox(smsMessageStr, address);
        }
    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }

}