package org.lepsms.ipooshsms;

/**
 * Created by andresteves on 04/09/2018.
 */

public class SmsObject {

    String smsMessage;
    String senderPhone;

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }
}
